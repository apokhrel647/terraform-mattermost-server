# terraform-mattermost-server
**************************************
Using the Concept of BASTION HOST and NAT INSTANCES (not a NAT Gateway) to create the Mattermost Server 
**************************************

We are using Ubuntu 18.04 instances and MySQL to create it.
**************************************
The Process is as follows:
**************************************
Create VPC
**************************************
Create Public Subnet (For Mattermost and NAT Instance)
**************************************
Create Private Subnet (For MySQL Server)
**************************************
Create Internet Gateway so that the Mattermost Server can reach to internet
**************************************
Create Public Route Table
**************************************
Associate Public Route Table with Public Subnet
**************************************
Create Security Group for NAT Instance
**************************************
Modify Ingress rule to allow SSH, HTTP and HTTPS for NAT instance
**************************************
Create NAT Instance on public subnet and its security group 
**************************************
If you are from different region than us-east-1, change your NAT Instance ami
**************************************
Create Private route table
**************************************
Associate your Private route table with Private Subnet
**************************************
Create a private key to log into Mattermost server instance
**************************************
We will be using the same key to log into MySQL server instance via Bastion Host (Mattermost server) for further installations
**************************************
Create a security group for Mattermost Server, allow Port 22 and 8065
**************************************
Port 22 will be used for SSH communication and port 8065 will be used to run Mattermost once the application is installed properly
**************************************
Create a security group for MySQL Database Server, allow Port 22 and 3306
**************************************
Port 22 will be used for SSH communication from the Bastion Host and port 3306 will be used by the SQL Server
**************************************
Create the Mattermost server and MySQL Database server with specified Security groups and subnets.
**************************************
Change the ami if you are from the different region than us-east-1
**************************************
See how the provisioners are used to configure on remote EC2 instances using the Bastion Host.
**************************************

To check if your Mattermost application is running or not, find the Public IP of Application Server (Mattermost server) and run it on port 8065

http://public_ip_mattermost_server:8065

**************************************
Notes: 
Since our purpose here is to learn about Bastion Host and NAT instances,
we are intentionally using very basic form of scripting here so that the main concept will remain in primary focus.

**************************************
Mattermost is an open source, private cloud, Slack-alternative from https://mattermost.com.
It's written in Golang and React and runs as a single Linux binary with MySQL or PostgreSQL.
Find more on: https://github.com/mattermost/mattermost-server
