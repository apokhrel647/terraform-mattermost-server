provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

# creating vpc
resource "aws_vpc" "my_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = {
    "Name" = "Project 1 VPC"
  }
}

# creating public subnet
resource "aws_subnet" "subnet_public" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "Public Subnet"
  }
}

# creating private subnet
resource "aws_subnet" "subnet_private" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.2.0/24"
  tags = {
    "Name" = "Private Subnet"
  }
}

# creating public facing internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    "Name" = "Project 1 Internet Gateway"
  }
}

# creating public route table 
resource "aws_route_table" "rt_public" {
  vpc_id = aws_vpc.my_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    "Name" = "Public Route Table"
  }
}

# assoicating public route table and public subnet
resource "aws_route_table_association" "rt_assoc" {
  subnet_id      = aws_subnet.subnet_public.id
  route_table_id = aws_route_table.rt_public.id
}

# creating security group for nat instance
resource "aws_security_group" "security-group-nat-instance" {
  vpc_id = aws_vpc.my_vpc.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }
  tags = {
    "Name" = "Security Group NAT Instance"
  }

}

# creating nat instance
# check your ami if your are not from us-east-1
resource "aws_instance" "nat_instance" {
  ami               = "ami-00a9d4a05375b2763"
  instance_type     = "t2.micro"
  subnet_id         = aws_subnet.subnet_public.id
  security_groups   = [aws_security_group.security-group-nat-instance.id]
  source_dest_check = false

  tags = {
    "Name" = "NAT Instance"
  }
}


# creating private route table
resource "aws_route_table" "rt_private" {
  vpc_id = aws_vpc.my_vpc.id
  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.nat_instance.id
  }

  tags = {
    "Name" = "Private Route Table"
  }
  depends_on = [aws_instance.nat_instance]
}

# associating private route table with private subnet
resource "aws_route_table_association" "rtb_assoc_private_subnet" {
  subnet_id      = aws_subnet.subnet_private.id
  route_table_id = aws_route_table.rt_private.id
  depends_on     = [aws_route_table.rt_private]
}

# creating private key to log into Application Server and Database Server instances 
# application server acts as Bastion Host to log into Database server 
resource "tls_private_key" "project1-key" {
  lifecycle {
    create_before_destroy = false
  }
  algorithm = "RSA"
}

#Save public key attributes from the generated key
resource "aws_key_pair" "App-Instance-Key" {
  key_name   = "project1-key"
  public_key = tls_private_key.project1-key.public_key_openssh
}

#Save the key to your local system
resource "local_file" "project1-key" {
  content  = tls_private_key.project1-key.private_key_pem
  filename = "project1-key.pem"
}

# creating security group for Application server
resource "aws_security_group" "security-group-app-server" {
  vpc_id = aws_vpc.my_vpc.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow http"
    from_port   = 8065
    to_port     = 8065
    protocol    = "tcp"
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }
  tags = {
    "Name" = "Security Group Application Server"
  }

}

# creating security group for Database server
resource "aws_security_group" "security-group-db-server" {
  vpc_id = aws_vpc.my_vpc.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow http"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }
  tags = {
    "Name" = "Security Group Database Server"
  }

}

# creating database server
# check your ami if you are not from us-east-1
resource "aws_instance" "db_server" {
  ami             = "ami-00ddb0e5626798373"
  instance_type   = "t2.micro"
  subnet_id       = aws_subnet.subnet_private.id
  security_groups = [aws_security_group.security-group-db-server.id]
  key_name        = "project1-key"

  tags = {
    "Name" = "Database Server"
  }

}

# creating application server (Mattermost server)
# check your ami if you are not from us-east-1
resource "aws_instance" "app_server" {
  depends_on = [aws_instance.db_server]

  ami             = "ami-00ddb0e5626798373"
  instance_type   = "t2.micro"
  subnet_id       = aws_subnet.subnet_public.id
  security_groups = [aws_security_group.security-group-app-server.id]
  key_name        = "project1-key"

  tags = {
    "Name" = "Application Server"
  }

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = tls_private_key.project1-key.private_key_pem
  }

  provisioner "file" {
    source      = "project1-key.pem"
    destination = "/home/ubuntu/project1-key.pem"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod 400 project1-key.pem",
      "sudo wget https://storage.googleapis.com/skl-training/aws-codelabs/mattermost/mattermost_install.sh",
      "sudo chmod 700 mattermost_install.sh",
    ]
  }
}

# creating null resource to copy keypair to the database server
# notice bastion_host=PublicIp of Application server inside connection to connect to MySQL DB server
# terraform connects to the mentioned host via bastion_host with supplied private_key as a specified user ubuntu
resource "null_resource" "connect_dbserver" {
  depends_on = [aws_instance.app_server, aws_instance.db_server]
  connection {
    bastion_host = aws_instance.app_server.public_ip
    host         = aws_instance.db_server.private_ip
    user         = "ubuntu"
    private_key  = tls_private_key.project1-key.private_key_pem
  }

  # copying the keypair file to DB server
  provisioner "file" {
    source      = "project1-key.pem"
    destination = "/home/ubuntu/project1-key.pem"
  }

  # downloading the required setup files on DB server
  provisioner "remote-exec" {
    inline = [
      "sudo chmod 400 project1-key.pem",
      "sudo wget https://storage.googleapis.com/skl-training/aws-codelabs/mattermost/install_mysql.sh",
      "sudo chmod 700 install_mysql.sh",
      "sudo ./install_mysql.sh"
    ]
  }
}

# creating null resource to connect to application server (Mattermost server) to run the downloaded setup files
resource "null_resource" "connect_appserver" {
  depends_on = [null_resource.connect_dbserver]
  connection {
    type        = "ssh"
    host        = aws_instance.app_server.public_ip
    user        = "ubuntu"
    private_key = tls_private_key.project1-key.private_key_pem
    timeout     = 60
  }

  provisioner "remote-exec" {
    inline = [
      "sudo ./mattermost_install.sh ${aws_instance.db_server.private_ip}",
      "sudo chown -R mattermost:mattermost /opt/mattermost",
      "sudo chmod -R g+w /opt/mattermost",
      "sudo -u mattermost /opt/mattermost/bin/mattermost",
      #"nohup sudo -u mattermost /opt/mattermost/bin/mattermost &",
    ]
  }
}

# displaying the public ip of mattermost server
output "app_server_public_ip" {
  value = "${aws_instance.app_server.public_ip}:8065"
}